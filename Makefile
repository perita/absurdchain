NULL =

PROJECT = absurdchain

FONTS = \
	ubuntu-titling \
	$(NULL)

FONT_TEXTURES := $(patsubst %,res/%.png,$(FONTS))
FONT_FNT := $(patsubst %.png,%.fnt,$(FONT_TEXTURES))

RESOURCES = \
	    $(FONT_FNT) \
	    $(FONT_TEXTURES) \
	    $(NULL)

HAXE_SRC := $(wildcard src/*.hx)

HAXE = haxe
HAXEHXML = $(PROJECT).hxml
HAXEPORT = 6000
HAXEHLHXML = $(PROJECT).hl.hxml
HAXEJSHXML = $(PROJECT).js.hxml
HLBIN = bin/$(PROJECT).hl
JSBIN = bin/$(PROJECT).js

ZIP = zip
ZIPFLAGS = -9 -j
ZIPBIN = bin/$(PROJECT).zip

define haxe_build
$(HAXE) --connect $(HAXEPORT) $(1) || ( [ $$? -eq 2 ] && $(HAXE) $(1) )
endef

$(HLBIN): $(HAXEHLHXML) $(HAXEHXML) $(HAXE_SRC) | $(RESOURCES)
	$(call haxe_build,$<)

$(JSBIN): $(HAXEJSHXML) $(HAXEHXML) $(HAXE_SRC) $(RESOURCES)
	$(call haxe_build,$<)

$(ZIPBIN): $(JSBIN) $(RESPAK) bin/index.html
	$(ZIP) $(ZIPFLAGS) $@ $^

res/%.png res/%.fnt: fonts.json assets/%.ttf
	fontgen $<
all: dist

dist: $(ZIPBIN)

watch:
	while true; do $(MAKE); inotifywait --recursive --event modify assets src Makefile; sleep 0.25; done

listen:
	haxe --server-listen $(HAXEPORT)

clean:
	rm -fr $(JSBIN) $(ZIPBIN) $(RESOURCES)

.PHONY: all dist run watch listen clean
