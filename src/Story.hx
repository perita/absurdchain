/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

typedef Part = {
  var teller: String;
  var mine: Bool;
  var content: String;
}

  enum Status {
    Pending;
    Started;
    Turn;
    Done;
  }

typedef Story = {
  var id: Int;
  var mine: Bool;
  var status: Status;
  var title: String;
  var tellers: Array<String>;
  var prev: String;
  var parts: Array<Part>;
}