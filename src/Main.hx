/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Main extends hxd.App {
  public static function main() {
    new Main();
  }

  public function new() {
    super();
    Main.ME = this;
  }

  override function loadAssets(onLoaded:Void->Void) {
    hxd.Res.initEmbed();
    onLoaded();
  }

  override function init() {
    s2d.scaleMode = LetterBox(375, 667);
    s2d.camera.clipViewport = true;
    engine.backgroundColor = 0xfafad2;

    doodle = new Doodle();

    font = hxd.Res.ubuntu_titling.toSdfFont(22, h2d.Font.SDFChannel.MultiChannel, 0.5, 4 / 24);
    bigFont = hxd.Res.ubuntu_titling.toSdfFont(30, h2d.Font.SDFChannel.MultiChannel, 0.5, 4 / 24);
    root = new h2d.Object(s2d);
    games = new Games(font, bigFont, root);

    client = new Client();

    client.onError = (msg) -> {
      if (alert != null) return;
      alert = new Alert(msg, font, null);
      s2d.add(alert, 2);
      alert.onDone = () -> {
	alert.remove();
	alert = null;
      }
    }

    client.onGameCreated = (id, title) -> {
      final story = games.onCreated(id, title);
      goToLobby(story);
    }

    client.onGameJoined = (id) -> {
      final story = stories[id];
      if (story == null) return;
      goToLobby(story);
    }

    var firstSync = true;
    client.onChanged = (changes) -> {
      var playedHorn = false;
      var playedMoney = firstSync;
      var playedWoohoo = firstSync;
      firstSync = false;
      for (c in changes) {
	if (c == null) continue;
	switch (c) {
	case Pending(id, mine, title, tellers):
	  final old = stories[id];
	  final story:Story = {
	  id: id,
	  mine: mine,
	  title: title,
	  status: Pending,
	  tellers: tellers,
	  prev: old?.prev ?? "",
	  parts: old?.parts ?? [],
	  };
	  stories[id] = story;
	  if (!playedMoney && tellers.length > 0) {
	    hxd.Res.money.play();
	    playedMoney = true;
	  }
	  if (lobby != null && lobby.story.id == story.id) {
	    lobby.story = story;
	  }
	case Started(id, mine, title, prev):
	  final old = stories[id];
	  final story:Story = {
	  id: id,
	  mine: mine,
	  title: title,
	  status: prev == "" ? Started : Turn,
	  tellers: old?.tellers ?? [],
	  prev: prev,
	  parts: old?.parts ?? [],
	  };
	  stories[id] = story;
	  if (story.status == Turn && !playedHorn) {
	    hxd.Res.horn.play();
	    playedHorn = true;
	  }
	  if (lobby != null && lobby.story.id == story.id) {
	    lobby.onBack();
	  }
	case Done(id, mine, title, parts):
	  final old = stories[id];
	  final story:Story = {
	  id: id,
	  mine: mine,
	  title: title,
	  status: Done,
	  tellers: old?.tellers ?? [],
	  prev: old?.prev ?? "",
	  parts: parts,
	  };
	  stories[id] = story;
	  if (!playedWoohoo) {
	    hxd.Res.woohoo.play();
	    playedWoohoo = true;
	  }
	}
      }
      games.refresh();
    }

    client.check();

    games.onNew = () -> {
      final prompt = new Prompt(playerName, false, font);
      s2d.add(prompt, 1);
      prompt.onAccept = (title, name) -> {
	playerName = name;
        client.createGame(name);
      }
      prompt.onDone = () -> {
	prompt.remove();
      }
    }

    games.onJoin = () -> {
      final prompt = new Prompt(playerName, true, font);
      s2d.add(prompt, 1);
      prompt.onAccept = (title, name) -> {
	playerName = name;
	client.join(title, name);
      }
      prompt.onDone = () -> {
	prompt.remove();
      }
    }

    games.onActivate = (i) -> {
      final story = stories[i];
      if (story == null) return;
      switch (story.status) {
      case Pending:
	goToLobby(story);
      case Started:
	// Nothing
      case Turn:
	goToWrite(story);
      case Done:
	goToRead(story);
      }
    }
  }

  function goToLobby(story:Story){
    removeScreens();
    lobby = new Lobby(story, font, bigFont, root);
    lobby.x = 385;
    lobby.onBack = () -> {
      toX = 0;
    }
    lobby.onSend = (story, text) -> {
      if (story.tellers.length == 0 || text.length == 0) return;
      client.write(story, text);
      toX = -lobby.x - 100;
      hxd.Res.boing.play();
    }
    toX = -lobby.x;
  }

  function goToRead(story:Story) {
    removeScreens();
    read = new Read(story, font, bigFont, root);
    read.x = 385;
    read.onBack = () -> {
      toX = 0;
    }
    toX = -read.x;
  }

  function goToWrite(story:Story) {
    removeScreens();
    write = new Write(story, font, bigFont, root);
    write.x = 385;
    write.onBack = () -> {
      toX = 0;
    }
    write.onSend = (story, text) -> {
      if (text.length == 0) return;
      client.write(story, text);
      toX = -write.x - 100;
      hxd.Res.boing.play();
    }
    toX = -write.x;
  }

  override function update(dt:Float) {
    final speedH = 375. * 4.;
    if (root.x > toX) {
      root.x = Math.max(toX, root.x - dt * speedH);
    } else if (root.x < toX) {
      root.x = Math.min(toX, root.x + dt * speedH);
    } else if (toX < -385) {
      toX = 0;
    } else if (Std.int(root.x) == 0) {
      removeScreens();
    }
  }

  function removeScreens() {
    if (read != null) {
      read.remove();
      read = null;
    }
    if (write != null) {
      write.remove();
      write = null;
    }
    if (lobby != null) {
      lobby.remove();
      lobby = null;
    }
  }

  public static var ME: Main;
  public var doodle: Doodle;
  public var client: Client;
  public final stories: Map<Int, Story> = [];
  var write: Write;
  var read: Read;
  var games: Games;
  var lobby: Lobby;
  var root: h2d.Object;
  var toX: Float = 0;
  var font: h2d.Font;
  var bigFont: h2d.Font;
  var alert:Alert;
  var playerName = "player";
}
