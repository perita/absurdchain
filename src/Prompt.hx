/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Prompt extends h2d.Object {
  public function new (pname:String, askTitle:Bool, font:h2d.Font, ?parent:h2d.Object) {
    super(parent);

    final back = new h2d.Interactive(375, 667, this);
    back.backgroundColor = 0x3f000000;
    back.onPush = (e) -> closing = true;

    g = new h2d.Graphics(this);
    g.addShader(Main.ME.doodle);

    final hole = new h2d.Interactive(0, 0, g);
    hole.cursor = Default;

    final nameLabel = new h2d.Text(font, g);
    nameLabel.x = 24;
    nameLabel.y = 24;
    nameLabel.textColor = 0xff000000;
    nameLabel.maxWidth = 290;
    nameLabel.text = "your name";
    h = Std.int(nameLabel.y * 2 + font.lineHeight);

    nameInput = new TextInput(font, g);
    nameInput.rich = false;
    nameInput.x = nameLabel.x;
    nameInput.y = h - 12;
    nameInput.maxWidth = nameLabel.maxWidth;
    nameInput.text = pname;
    h += Std.int(font.lineHeight + 24);

    if (askTitle) {
      final titleLabel = new h2d.Text(font, g);
      titleLabel.x = nameLabel.x;
      titleLabel.y = h;
      titleLabel.textColor = 0xff000000;
      titleLabel.maxWidth = nameLabel.maxWidth;
      titleLabel.text = "story title";
      h += Std.int(24 + font.lineHeight);

      titleInput = new TextInput(font, g);
      titleInput.rich = false;
      titleInput.x = nameLabel.x;
      titleInput.y = h - 12;
      titleInput.maxWidth = titleLabel.maxWidth;
      h += Std.int(font.lineHeight + 48);
    } else {
      titleInput = null;
    }

    inline function drawInputBorder(input:TextInput, color:Int) {
      final iw = input.maxWidth + 6;
      final ih = input.textHeight + 6;
      final x = input.x - 3;
      final y = input.y - 3;
      g.beginFill(color);
      g.drawRect(x, y, iw, ih);
      g.endFill();
      g.lineStyle(3);
      g.moveTo(x + 6, y);
      g.lineTo(x + iw, y);
      g.lineTo(x + iw, y + ih);
      g.moveTo(x + iw - 3, y + ih);
      g.lineTo(x, y + ih);
      g.lineTo(x, y);
    }

    h += 48;
    final w = nameLabel.x * 2 + nameLabel.maxWidth;
    c = Std.int(667 / 3 - h / 2);

    inline function drawBorder() {
      g.clear();
      g.x = 375 / 2 - w / 2;
      g.beginFill(0xffffffff);
      g.drawRect(0, 0, w, h);
      g.endFill();
      drawInputBorder(nameInput, 0xffba9366);
      if (titleInput != null) {
	drawInputBorder(titleInput, 0xffba9366);
      }
      g.lineStyle(3, 0xffdc3545);
      g.moveTo(50, h - 50);
      g.lineTo(25, h - 25);
      g.moveTo(50, h - 25);
      g.lineTo(25, h - 50);

      g.lineStyle(3, 0xff28a745);
      g.moveTo(w - 50, h - 37);
      g.lineTo(w - 37, h - 25);
      g.lineTo(w - 25, h - 50);
    }
    drawBorder();

    final nameChange = nameInput.onChange;
    nameInput.onChange = () -> {
      nameChange();
      nameInput.text = nameInput.text.substr(0, 20);
      drawBorder();
    }
    if (titleInput != null) {
      final titleChange = titleInput.onChange;
      titleInput.onChange = () -> {
	titleChange();
	titleInput.text = titleInput.text.substr(0, 30);
	drawBorder();
      }
    }

    hole.width = w;
    hole.height = h;

    final cancel = new h2d.Interactive(50, 50, g);
    cancel.x = 12;
    cancel.y = h - 64;
    cancel.onPush = (e) -> closing = true;

    final ok = new h2d.Interactive(50, 50, g);
    ok.x = w - 64;
    ok.y = h - 64;
    ok.onPush = (e) -> {
      if (nameInput.text.length > 0 && (titleInput?.text ?? "tile").length > 0) {
	accept = true;
	closing = true;
      }
    }
  }

  public dynamic function onDone() {
  }

  public dynamic function onAccept(title:String, name:String) {
  }

  override function sync(ctx:h2d.RenderContext) {
    super.sync(ctx);

    if (closing) {
      if (g.y < -h) {
	if (accept) {
	  onAccept(titleInput?.text ?? "", nameInput.text);
	}
	onDone();
      } else {
	g.y -= 667 * 2 * ctx.elapsedTime;
      }
    } else if (g.y < c) {
      g.y = Math.min(g.y + 667 * 2 * ctx.elapsedTime, c);
    }
  }

  var h = 0;
  var c = 0;
  var closing = false;
  var accept = false;
  final nameInput:TextInput;
  final titleInput:TextInput;
  final g:h2d.Graphics;
}