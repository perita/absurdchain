/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Bounce extends hxsl.Shader {

  static var SRC = {
    @global var time : Float;
    var output : {
      var position : Vec4;
      var color : Vec4;
    };

    @param var intensity: Float = 0.01;
    
    function vertex() {
      var t = snap(time, 0.25);
      output.position.y += abs(sin(output.position.x * 10 + time * 4) * intensity);
    }

    function snap(x:Float, snap:Float):Float {
      return snap * floor(x / snap);
    }
  };
}
