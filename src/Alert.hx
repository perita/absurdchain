/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Alert extends h2d.Object {
  public function new (msg: String, font:h2d.Font, ?parent:h2d.Object) {
    super(parent);

    final back = new h2d.Interactive(375, 667, this);
    back.backgroundColor = 0x3f000000;
    back.onPush = (e) -> closing = true;

    g = new h2d.Graphics(this);
    g.addShader(Main.ME.doodle);

    final text = new h2d.Text(font, g);
    text.x = 24;
    text.y = 24;
    text.textColor = 0xff000000;
    text.maxWidth = 250;
    text.textAlign = Center;
    text.text = msg;

    final w = text.x * 2 + text.maxWidth;
    h = Std.int(text.y * 2 + text.textHeight + 25);
    c = Std.int(667 / 3 - h / 2);
    g.clear();
    g.x = 375 / 2 - w / 2;
    g.beginFill(0xffffffff);
    g.drawRect(0, 0, w, h);
    g.endFill();
    g.lineStyle(3, 0xffdc3545);
    g.moveTo(w - 40, h - 40);
    g.lineTo(w - 15, h - 15);
    g.moveTo(w - 40, h - 15);
    g.lineTo(w - 15, h - 40);
  }

  public dynamic function onDone() {
  }

  override function sync(ctx:h2d.RenderContext) {
    super.sync(ctx);

    if (closing) {
      if (g.y < -h) {
	onDone();
      } else {
	g.y -= 667 * 2 * ctx.elapsedTime;
      }
    } else if (g.y < c) {
      g.y = Math.min(g.y + 667 * 2 * ctx.elapsedTime, c);
    }
  }

  var h = 0;
  var c = 0;
  var closing = false;
  final g:h2d.Graphics;
}