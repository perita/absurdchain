/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

private typedef StoryChunk = { who: String, what: String };

class Read extends h2d.Object {
  public function new (story: Story, font: h2d.Font, bigFont: h2d.Font, ?parent:h2d.Object) {
    super(parent);

    final g = new h2d.Graphics(this);
    g.x = 2;
    g.y = 2;
    g.addShader(Main.ME.doodle);

    final w = 365.;
    final title = new h2d.Text(bigFont, g);
    title.x = 25;
    title.y = 5;
    title.maxWidth = w - title.x - 5;
    title.textAlign = Center;
    title.text = story.title;
    title.textColor = 0xff000000;

    final h = title.y * 2 + title.textHeight;
    g.clear();
    g.beginFill(0xffffdecb);
    g.drawRect(0, 0, w, h);
    g.endFill();
    g.lineStyle(3);
    g.moveTo(6, 0);
    g.lineTo(w, 0);
    g.lineTo(w, h);
    g.moveTo(w - 3, h);
    g.lineTo(0, h);
    g.lineTo(0, 0);
    g.beginFill(0xff61689e);
    g.moveTo(5, h / 2);
    g.lineTo(20, h / 2 - 17);
    g.lineTo(20, h / 2 + 17);
    g.lineTo(5, h / 2);
    g.endFill();

    flow = new h2d.Flow(this);
    flow.y = h + 5;
    flow.padding = 2;
    flow.verticalSpacing = 5;
    flow.horizontalAlign = Left;
    flow.verticalAlign = Top;
    flow.layout = Vertical;
    flow.overflow = Scroll;
    flow.minWidth = flow.maxWidth = 375;
    flow.minHeight = flow.maxHeight = 667 - Std.int(flow.y);

    for (part in story.parts) {
      final who = new h2d.Text(font, flow);
      flow.getProperties(who).paddingTop = 10;
      who.addShader(Main.ME.doodle);
      who.text = part.teller + ":";
      who.textColor = part.mine ? 0xff000000 : 0xff666666;

      final what = new TextInput(font, flow);
      flow.getProperties(what).paddingLeft = 20;
      what.backgroundColor = part.mine ? 0xff66ba66 : 0xffba9366;
      what.maxWidth = 365 - 20 - 7;
      what.canEdit = false;
      what.text = part.content;
    }

    final fin = new h2d.Text(font, flow);
    flow.getProperties(fin).paddingTop = 20;
    flow.getProperties(fin).paddingBottom = 20;
    flow.getProperties(fin).horizontalAlign = Middle;
    fin.addShader(Main.ME.doodle);
    fin.text = " — FIN — ";
    fin.textColor = 0xff000000;

    final interactive = new h2d.Interactive(15, 17 * 2, g);
    interactive.x = 5;
    interactive.y = h / 2 - 17;
    interactive.onPush = (e) -> onBack();
  }

  public dynamic function onBack() {
  }

  var story:Array<StoryChunk>;
  final flow:h2d.Flow;
}
