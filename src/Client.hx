/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */
using haxe.Http;

class Client {
  public function new() {
  }

  public function check() {
    final request = createRequest();
    request.setPostData('CHECK\t${lastTimestamp}');
    request.onData = parseCheckResponse;
    request.request(true);
  }

  function parseCheckResponse(data: String) {
    final changes:Array<Change> = [];
    final lines = data.split("\n");
    for (line in lines) {
      final values = line.split("\t");
      switch (values[0]) {
      case "KO":
	onError(values[1]);
	check();
	return;
      case "OK":
	lastTimestamp = Std.parseInt(values[1]);
	if (changes.length > 0) {
	  onChanged(changes);
	}
	check();
	return;
      default:
	if (values.length > 4) {
	  final storyId = Std.parseInt(values[0]);
	  final mine = StringTools.trim(values[1]) == "M";
	  final title = StringTools.trim(values[2]);
	  final rest = StringTools.trim(values[4]);
	  changes.push(switch(values[3]) {
	    case "P": Pending(storyId, mine, title, rest.split("\x1e"));
	    case "S": Started(storyId, mine, title, rest);
	    case "D": Done(storyId, mine, title, rest.split("\x1e").map((s) -> {
	      final v = s.split("\x1f");
	      return {
	      teller: StringTools.trim(v[0]),
	          mine: StringTools.trim(v[1]) == "M",
	          content: StringTools.trim(v[2]),
	          };
	      }));
	    default: null;
	    });
	}
      }
    }
  }

  public function write(story:Story, text:String) {
    final request = createRequest();
    request.setPostData('WRITE\t${story.id}\t${text}');
    request.onData = (data) -> parseWriteResponse(story, data);
    request.request(true);
  }

  public function parseWriteResponse(story:Story, data:String) {
    final values = data.split("\t");
    if (values[0] == "KO") {
      onError(values[1]);
      return;
    }
    onChanged([Started(story.id, story.mine, story.title, "")]);
  }

  public function createGame(playerName: String) {
    final request = createRequest();
    request.setPostData('CREATE\t${playerName}');
    request.onData = parseCreateResponse;
    request.request(true);
  }

  function parseCreateResponse(data: String) {
    final values = data.split("\t");
    if (values[0] == "KO") {
      onError(values[1]);
      return;
    }
    final id = Std.parseInt(values[1]);
    final title = StringTools.trim(values[2]);
    onGameCreated(id, title);
  }

  public function join(storyTitle: String, playerName: String) {
    final request = createRequest();
    request.setPostData('JOIN\t${storyTitle}\t${playerName}');
    request.onData = parseJoinResponse;
    request.request(true);
  }

  function parseJoinResponse(data: String) {
    final values = data.split("\t");
    if (values[0] == "KO") {
      onError(values[1]);
      return;
    }
    final id = Std.parseInt(values[1]);
    final title = StringTools.trim(values[2]);
    onChanged([Pending(id, false, title, [])]);
    onGameJoined(id);
  }

  function createRequest(): Http {
    final request = new Http(url);
    request.withCredentials = true;
    request.setHeader("Content-Type", "application/x.absurdchain");
    request.onError = onError;
    request.onData = (data) -> trace('data', data);
    return request;
  }

  public dynamic function onError(msg: String) {
  }

  public dynamic function onGameCreated(id: Int, title: String) {
  }

  public dynamic function onGameJoined(id: Int) {
  }

  public dynamic function onChanged(changes:Array<Change>) {
  }

  final url = "https://absurdchain.peritasoft.com/";
  var lastTimestamp = 0;
}

enum Change {
  Pending(id:Int, mine:Bool, title:String, tellers:Array<String>);
  Started(id:Int, mine:Bool, title:String, lastPart:String);
  Done(id:Int, mine:Bool, title:String, parts:Array<Story.Part>);
}
