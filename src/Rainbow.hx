/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Rainbow extends hxsl.Shader {

  static var SRC = {
    var pixelColor : Vec4;
    @global var time : Float;
    var output : {
      var position : Vec4;
      var color : Vec4;
    };

    function fragment() {
      var t = snap(time, 0.25);

      pixelColor.rgb *= vec3(.5*cos(6.283*(output.position.x*10+time+vec3(0.,-.33333,.33333)))+.75);
    }
    
    function snap(x:Float, snap:Float):Float {
      return snap * floor(x / snap);
    }
  };
}
