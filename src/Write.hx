/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Write extends h2d.Object {
  public function new (story: Story, font: h2d.Font, bigFont: h2d.Font, ?parent:h2d.Object) {
    super(parent);

    final g = new h2d.Graphics(this);
    g.x = 2;
    g.y = 2;
    g.addShader(Main.ME.doodle);

    final w = 365.;
    final title = new h2d.Text(bigFont, g);
    title.x = 25;
    title.y = 5;
    title.maxWidth = w - title.x - 5;
    title.textAlign = Center;
    title.text = story.title;
    title.textColor = 0xff000000;

    final h = title.y * 2 + title.textHeight;

    final input = new TextInput(font, this);
    input.x = 10;
    input.y = 357;
    input.maxWidth = w - input.x;

    final prev = new h2d.Text(font, this);
    prev.x = 5;
    prev.y = title.y + h + 24;
    prev.text = "Last part:";
    prev.textColor = 0xff000000;

    final what = new TextInput(font, this);
    what.x = prev.x;
    what.y = prev.y + font.lineHeight + 6;
    what.maxWidth = w - what.x;
    what.canEdit = false;
    what.text = story.prev;

    inline function drawInputBorder(input:h2d.TextInput, color:Int) {
      final iw = input.maxWidth + 6;
      final ih = input.textHeight + 6;
      final x = input.x - g.x - 3;
      final y = input.y - g.y - 3;
      g.beginFill(color);
      g.drawRect(x, y, iw, ih);
      g.endFill();
      g.lineStyle(3);
      g.moveTo(x + 6, y);
      g.lineTo(x + iw, y);
      g.lineTo(x + iw, y + ih);
      g.moveTo(x + iw - 3, y + ih);
      g.lineTo(x, y + ih);
      g.lineTo(x, y);
    }

    inline function drawBorder() {
      g.clear();
      g.beginFill(0xffffdecb);
      g.drawRect(0, 0, w, h);
      g.endFill();
      g.lineStyle(3);
      g.moveTo(6, 0);
      g.lineTo(w, 0);
      g.lineTo(w, h);
      g.moveTo(w - 3, h);
      g.lineTo(0, h);
      g.lineTo(0, 0);
      g.beginFill(0xff61689e);
      g.moveTo(5, h / 2);
      g.lineTo(20, h / 2 - 17);
      g.lineTo(20, h / 2 + 17);
      g.lineTo(5, h / 2);
      g.endFill();

      g.beginFill(0xffc9cc78);
      g.moveTo(330, 590);
      g.lineTo(330, 640);
      g.lineTo(360, 615);
      g.lineTo(330, 590);
      g.endFill();

      drawInputBorder(input, 0xff66ba66);
      drawInputBorder(what, 0xffba9366);
    }
    drawBorder();

    final debug = new h2d.Text(font, this);
    debug.textColor = 0xff000000;
    debug.setPosition(input.x, input.y - font.lineHeight - 6);
    debug.maxWidth = w - debug.x;
    debug.textAlign = Right;
    debug.text = '${input.text.length} / 160';

    final prompt = new h2d.Text(font, this);
    prompt.setPosition(debug.x, debug.y);
    prompt.textColor = 0xff000000;
    prompt.text = "Write continuation here";

    final change = input.onChange;
    input.onChange = () -> {
      change();
      drawBorder();
      debug.text = '${input.text.length} / 160';
    }

    final send = new h2d.Interactive(50, 50, g);
    send.setPosition(330, 590);
    send.onPush = (e) -> onSend(story, input.text);

    final interactive = new h2d.Interactive(15, 17 * 2, g);
    interactive.x = 5;
    interactive.y = h / 2 - 17;
    interactive.onPush = (e) -> onBack();
  }

  public dynamic function onBack() {
  }

  public dynamic function onSend(story:Story, text:String) {
  }
}