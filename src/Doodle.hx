/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Doodle extends hxsl.Shader {
  static var SRC = {
    @:import h3d.shader.Base2d;
    
    function vertex() {
      var t = snap(time, 0.25);
      var noise = doodleOffset(output.position.xy + t + 1);
      output.position.xy += noise.xy;
    }
    
    function snap(x:Float, snap:Float):Float {
      return snap * floor(x / snap);
    }
    
    function random(seed:Vec2):Float
    {
      return fract(sin(dot(seed.xy, vec2(12.9898, 78.233))) * 43758.5453123);
    }

    function lerp(a:Float, b:Float, w:Float):Float {
      return a + w * (b - a);
    }

    function noise(seed:Vec2):Float
    {
      var i = floor(seed);
      var f = fract(seed);

      var a = random(i);
      var b = random(i + vec2(1.0, 0.0));
      var c = random(i + vec2(0.0, 1.0));
      var d = random(i + vec2(1.0, 1.0));

      var u = f* f * (3.0 - 2.0 * f);

      return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
    }

    function doodleOffset(textureCoords: Vec2): Vec2
    {
      var offset = vec2(0, 0);
      offset.x = (noise(textureCoords * 5) * 2.0 - 1.0) * 0.005;
      offset.y = (noise(textureCoords * 5) * 2.0 - 1.0) * 0.005;
      return offset;
    }
  };
}
