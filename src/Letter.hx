/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Letter extends h2d.Bitmap {
  override function sync(ctx:h2d.RenderContext) {
    super.sync(ctx);
    rotation += ctx.elapsedTime * 20;
    if (delay > 0) {
      delay -= ctx.elapsedTime;
    } else {
      x -= ctx.elapsedTime * 50;
      up -= ctx.elapsedTime * 80;
      y -= up * ctx.elapsedTime * 5;
      if (y > 1000) {
	remove();
      }
    }
  }

  var up = 50.0;
  public var delay = 0.0;
}
