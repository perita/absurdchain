/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class Games extends h2d.Object {
  public function new(font:h2d.Font, bigFont:h2d.Font, ?parent:h2d.Object) {
    super(parent);
    this.font = font;

    final title = new h2d.Text(bigFont, this);
    title.y = 12;
    title.textColor = 0xff61689e;
    title.text = "my stories";
    title.textAlign = Center;
    title.maxWidth = 375;

    list = new h2d.Flow(this);
    list.x = 5;
    list.y = title.y + title.textHeight + 24 + 3;
    list.minWidth = list.maxWidth = 361;
    list.minHeight = list.maxHeight = 527 - 6;
    list.verticalSpacing = 5;
    list.horizontalAlign = Left;
    list.verticalAlign = Top;
    list.layout = Vertical;
    list.overflow = Scroll;

    final g = new h2d.Graphics(this);
    g.addShader(Main.ME.doodle);
    g.x = 3;
    g.y = list.y - 3;
    g.clear();
    g.lineStyle(3);
    g.moveTo(0, 0);
    g.lineTo(365, 0);
    g.lineTo(365, 527);
    g.moveTo(360, 527);
    g.lineTo(0, 527);
    g.lineTo(0, 4);
    g.moveTo(300, 540);
    g.lineTo(300, 590);
    g.moveTo(280, 565);
    g.lineTo(320, 565);
    g.moveTo(65, 540);
    g.lineTo(50, 570);
    g.lineTo(55, 590);
    g.lineTo(75, 580);
    g.lineTo(90, 550);
    g.lineTo(65, 540);
    g.moveTo(50, 570);
    g.lineTo(75, 580);

    final add = new h2d.Interactive(50, 50, this);
    add.setPosition(280, 608);
    add.onPush = (e) -> onNew();

    final join = new h2d.Interactive(50, 50, this);
    join.setPosition(45, 608);
    join.onPush = (e) -> onJoin();
  }

  public function onCreated(id: Int, title: String): Story {
    var story = Main.ME.stories[id];
    if (story == null) {
      story = {
      id: id,
      mine: true,
      title: title,
      status: Pending,
      tellers: [],
      prev: "",
      parts: [],
      };
    } else {
      story.title = title;
      story.mine = true;
    }
    Main.ME.stories[id] = story;
    refresh();
    return story;
  }

  public function refresh() {
    final stories = [for (story in Main.ME.stories) story];
    stories.sort((a, b) -> a.title < b.title ? -1 : a.title > b.title ? 1 : 0);
    list.removeChildren();
    for (story in stories) {
      final item = new Game(story, font, list);
      item.onActivate = () -> {
	onActivate(story.id);
      }
    }
  }

  public dynamic function onActivate(gameId:Int) {
  }

  public dynamic function onNew() {
  }

  public dynamic function onJoin() {
  }

  final list: h2d.Flow;
  final font: h2d.Font;
}

private class Game extends h2d.Interactive {
  public function new (story:Story, font:h2d.Font, ?parent:h2d.Object) {
    super(347, font.lineHeight * 2 + 12, parent);
    onPush = (e) -> onActivate();

    final n = new h2d.Text(font, this);
    n.x = 3;
    n.y = 3;
    n.textColor = 0xff000000;
    n.text = story.title;

    final s = new h2d.Text(font, this);
    s.x = n.x;
    s.y = n.y + n.textHeight + 6;
    s.textColor= switch(story.status) {
    case Pending: 0xff17a2b8;
    case Started: 0xff888888;
    case Turn: 0xffdc3545;
    case Done: 0xff28a748;
    };
    s.text = switch (story.status) {
    case Pending: "waiting for players";
    case Started: "in play";
    case Turn: "your turn";
    case Done: "finished";
    };
  }

  public dynamic function onActivate() {
  }
}