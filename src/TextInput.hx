/*
 * SPDX-FileCopyrightText: © 2023 jordi fita mas <jfita@peritasoft.com>
 * SPDX-License-Identifier: GPL-3.0-only
 */

class TextInput extends h2d.TextInput {
  public var rich = true;

  public function new(font:h2d.Font, ?parent:h2d.Object) {
    super(font, parent);
    textColor = 0xffffff;
    dropShadow = { dx: 2, dy: 2, color: 0x00000000, alpha: 1.0 };
  }

  override function set_text(t : String) {
    super.set_text(t.substr(0, 160));
    return t;
  }

  override function initGlyphs(text : String, rebuild = true ): Void {
    if (rebuild) {
      glyphs.clear();
      while(elements.length > 0) {
	elements.pop().remove();
      }
    }
    var x = 0., y = 0., xMax = 0., xMin = 0., yMin = 0., prevChar = -1, linei = 0;
    var align = textAlign;
    var lines = new Array<Float>();
    var dl = font.lineHeight + lineSpacing;
    var t = splitRawText(text, 0, 0, lines);

    for ( lw in lines ) {
      if ( lw > x ) x = lw;
    }
    calcWidth = x;

    switch( align ) {
    case Center, Right, MultilineCenter, MultilineRight:
      var max = if( align == MultilineCenter || align == MultilineRight ) hxd.Math.ceil(calcWidth) else realMaxWidth < 0 ? 0 : hxd.Math.ceil(realMaxWidth);
	var k = align == Center || align == MultilineCenter ? 0.5 : 1;
	for( i in 0...lines.length )
	  lines[i] = Math.ffloor((max - lines[i]) * k);
	x = lines[0];
	xMin = x;
    case Left:
      x = 0;
    }

    var lc = -1;
    var prevGlyphs = null;
    for( i in 0...t.length ) {
      var cc = t.charCodeAt(i);
      var back = false;
      if (rich && prevGlyphs == null && (cc == '*'.code || cc == '_'.code)) {
	lc = cc;
	prevGlyphs = glyphs;
	glyphs = new h2d.TileGroup(font == null ? null : font.tile, this);
	glyphs.name = cc == '*'.code ? "rainbow" : "bouncy";
	glyphs.smooth = this.smooth;
	glyphs.addShader(this.sdfShader);
	glyphs.addShader(cc == '*'.code ? new Rainbow() : new Bounce());
	elements.push(glyphs);
      }else if (prevGlyphs != null && cc == lc) {
	back = true;
      }
      var e = font.getChar(cc);
      var offs = e.getKerningOffset(prevChar);
      var esize = e.width + offs;
      // if the next word goes past the max width, change it into a newline

      if( cc == '\n'.code ) {
	if( x > xMax ) xMax = x;
	switch( align ) {
	case Left:
	  x = 0;
	case Right, Center, MultilineCenter, MultilineRight:
	  x = lines[++linei];
	  if( x < xMin ) xMin = x;
	}
	y += dl;
	prevChar = -1;
      } else {
	if( e != null ) {
	  if( rebuild ) glyphs.add(x + offs, y, e.t);
	  if( y == 0 && e.t.dy < yMin ) yMin = e.t.dy;
	  x += esize + letterSpacing;
	}
	prevChar = cc;
      }
      if (back) {
	glyphs = prevGlyphs;
	prevGlyphs = null;
      }
    }
    if (prevGlyphs != null) {
      glyphs = prevGlyphs;
    }
    if( x > xMax ) xMax = x;

    calcXMin = xMin;
    calcYMin = yMin;
    calcWidth = xMax - xMin;
    calcHeight = y + font.lineHeight;
    calcSizeHeight = y + (font.baseLine > 0 ? font.baseLine : font.lineHeight);
    calcDone = true;
    if ( rebuild ) {
      needsRebuild = false;
      this.calcWidth += cursorTile.width; // cursor end pos
      if( inputWidth != null && this.calcWidth > inputWidth ) this.calcWidth = inputWidth;
    }
  }

  override function draw(ctx:h2d.RenderContext) {
    super.draw(ctx);
    if( dropShadow != null ) {
      var oldX = absX, oldY = absY;
      absX += dropShadow.dx * matA + dropShadow.dy * matC;
      absY += dropShadow.dx * matB + dropShadow.dy * matD;
      var oldR = color.r;
      var oldG = color.g;
      var oldB = color.b;
      var oldA = color.a;
      color.setColor(dropShadow.color);
      color.a = dropShadow.alpha * oldA;
      if (shadowBounce == null) {
	shadowBounce = new Bounce();
	shadowBounce.intensity = 0.005;
      }
      for(g in elements) {
	final bounce = g.name == "bouncy";
	if (bounce) addShader(shadowBounce);
	@:privateAccess g.drawWith(ctx, this);
	if (bounce) removeShader(shadowBounce);
      }
      absX = oldX;
      absY = oldY;
      color.set(oldR, oldG, oldB, oldA);
    }
  }

  override function beforeChange() {
    prev = text;
    super.beforeChange();
  }

  override function onChange() {
      final removed = getRemoved(prev, text);
      final p = localToGlobal();
      var x = 0.0;
      var delay = 0.01 * (removed.length - 1);
      for(c in removed) {
	final e = font.getChar(prev.charCodeAt(c));
	final t = e.t.clone();
	t.setCenterRatio();
	final l = new Letter(t, Main.ME.s2d);
	l.color.setColor(0xFF000000);
	l.delay = delay;
	l.addShader(sdfShader);
	l.setPosition(p.x + cursorX + x, p.y + cursorY);
	x += e.width;
	delay -= 0.01;
      }
  }

  function getRemoved(a:String, b:String) {
    if (a.length <= b.length) {
      return [];
    }
    final removed:Array<Int> = [];
    var i = 0;
    var j = 0;
    while (i < a.length && j < b.length) {
      var ac = a.charAt(i);
      var bc = b.charAt(j);
      if (ac == bc) {
	i++; j++;
      } else {
	do {
	  removed.push(i++);
	  ac = a.charAt(i);
	} while(ac != bc && i < a.length);
      }
    }
    while (i < a.length) {
      removed.push(i++);
    }
    return removed;
  }

  final elements: Array<h2d.TileGroup> = [];
  var shadowBounce: Bounce;
  var prev: String;
}