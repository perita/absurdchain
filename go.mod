module peritasoft.com/absurdchain

go 1.19

require (
	github.com/mattn/go-sqlite3 v1.14.17
	golang.org/x/text v0.11.0
)
