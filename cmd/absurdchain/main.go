package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/mattn/go-sqlite3"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"path"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"
)

const (
	sessionCookie = "absurd-session"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	db, err := newDatabase("/var/lib/absurdchain/stories.db")
	if err != nil {
		fatalf("failed to open database: %w", err)
	}
	defer db.Close()

	commands := map[string]func(*DB){
		"init":  createSchema,
		"serve": serve,
	}

	commands["help"] = func(_ *DB) {
		fmt.Println("Available commands:")
		for cmd := range commands {
			fmt.Printf("  %s\n", cmd)
		}
	}

	command := "serve"
	if len(os.Args) > 1 {
		command = os.Args[1]
	}
	fn, ok := commands[command]
	if !ok {
		fatalf("unknown command: %s", command)
	}
	fn(db)
}

func newDatabase(path string) (*DB, error) {
	db, err := sql.Open("sqlite3", path+"?_foreign_keys=on")
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

type DB struct {
	*sql.DB
}

func (db *DB) CreateSchema(ctx context.Context) error {
	_, err := db.ExecContext(ctx, `
create table story (
    story_id integer primary key,
    title text not null,
    status text not null default 'P' check (status in ('P', 'S', 'D')),
    created_at datetime not null default current_timestamp,
    updated_at datetime not null default current_timestamp,
    created_by text not null
);

create unique index waiting_title on story(title) where status = 'P';

create table storyteller (
    storyteller_id integer primary key,
    name text not null,
    uuid text not null,
    story_id integer not null references story,
    unique (story_id, uuid)
);

create table part(
    part_id integer primary key,
    seq integer not null,
    storyteller_id integer not null references storyteller,
    content text check( content is null or length(content) <= 160 ),
    written_at datetime check ((content is null) = (written_at is null))
)
`)
	return err
}

func createSchema(db *DB) {
	if err := db.CreateSchema(context.Background()); err != nil {
		fatalf("could not create schema: %w", err)
	}
}

func serve(db *DB) {
	srv := http.Server{
		Addr:         ":8080",
		Handler:      newHandler(db),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  20 * time.Minute,
	}

	go func() {
		log.Printf("INFO - listening on %s\n", srv.Addr)
		if err := srv.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			fatalf("http server error: %w", err)
		}
	}()

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
	<-sigCh
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	log.Print("INFO - stopping server")
	if err := srv.Shutdown(ctx); err != nil {
		fatalf("failed to stop server: %w", err)
	}
}
func newHandler(db *DB) http.Handler {
	var handler http.Handler = &storyHandler{
		db:    db,
		caser: cases.Title(language.English),
	}
	handler = recoverPanic(handler)
	return handler
}

type storyHandler struct {
	db    *DB
	caser cases.Caser
}

func (db *DB) MustGetString(ctx context.Context, query string, args ...interface{}) string {
	row := db.QueryRowContext(ctx, query, args...)
	if row.Err() != nil {
		panic(row.Err())
	}
	var result string
	if err := row.Scan(&result); err != nil {
		panic(err)
	}
	return result
}

func (db *DB) MustExec(ctx context.Context, query string, args ...interface{}) sql.Result {
	result, err := db.ExecContext(ctx, query, args...)
	if err != nil {
		panic(err)
	}
	return result
}

func (db *DB) MustGetInt(ctx context.Context, query string, args ...interface{}) int {
	row := db.QueryRowContext(ctx, query, args...)
	if row.Err() != nil {
		panic(row.Err())
	}
	var result int
	if err := row.Scan(&result); err != nil {
		panic(err)
	}
	return result
}

func (db *DB) MustGetDataVersion(ctx context.Context, conn *sql.Conn) int {
	row := conn.QueryRowContext(ctx, "PRAGMA data_version")
	if row.Err() != nil {
		panic(row.Err())
	}
	var result int
	if err := row.Scan(&result); err != nil {
		panic(err)
	}
	return result
}

func (h *storyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)
	switch head {
	case "":
		origin := r.Header.Get("origin")
		if origin != "" {
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Headers", "Cookie, Content-Type")
			w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
			w.Header().Set("Access-Control-Allow-Origin", origin)
		}

		switch r.Method {
		case http.MethodPost:
			var uid string
			if cookie, err := r.Cookie(sessionCookie); err == nil {
				uid = cookie.Value
			} else {
				uid = h.db.MustGetString(r.Context(), "select hex(randomblob(32))")
				setSessionCookie(w, uid)
			}

			if r.Header.Get("Content-Type") != "application/x.absurdchain" {
				http.Error(w, "415 Unsupported Media Type", http.StatusUnsupportedMediaType)
				return
			}
			r.Body = http.MaxBytesReader(w, r.Body, 4096)
			content, err := io.ReadAll(r.Body)
			if err != nil {
				panic(err)
			}
			defer r.Body.Close()
			fields := strings.Split(string(content), "\t")
			if len(fields) == 1 {
				h.error(w, "Invalid number of parameters")
				return
			}
			w.Header().Set("Content-Type", "application/x.absurdchain")
			switch fields[0] {
			case "JOIN":
				h.handleJoin(r.Context(), w, uid, fields[1:])
			case "CREATE":
				h.handleCreate(r.Context(), w, uid, fields[1:])
			case "WRITE":
				h.handleWrite(r.Context(), w, uid, fields[1:])
			case "CHECK":
				h.handleCheck(r.Context(), w, uid, fields[1:])
			}
		case http.MethodOptions:
			w.WriteHeader(http.StatusNoContent)
		default:
			methodNotAllowed(w, r, http.MethodPost, http.MethodOptions)
		}
	default:
		http.NotFound(w, r)
	}
}

func (h *storyHandler) error(w http.ResponseWriter, error string) {
	w.Header().Set("Content-Type", "application/x.absurdchain")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	fmt.Fprintln(w, "KO\t"+error)
}

func (h *storyHandler) success(w http.ResponseWriter, result string) {
	w.Header().Set("Content-Type", "application/x.absurdchain")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	fmt.Fprintln(w, "OK\t"+result)
}

func (h *storyHandler) handleJoin(ctx context.Context, w http.ResponseWriter, uid string, params []string) {
	if (len(params)) != 2 {
		h.error(w, "Need story title and storyteller name")
		return
	}

	title := h.caser.String(strings.TrimSpace(params[0]))
	storyId := h.db.MustGetInt(ctx, "select story_id from story where status = 'P' and title = ?1 union all select 0", title)
	if storyId == 0 {
		h.error(w, "Story not found")
		return
	}

	name := params[1]
	if len(name) == 0 {
		h.error(w, "Name can not be empty")
		return
	}
	h.db.MustExec(ctx, `
		insert into storyteller(name, uuid, story_id)
		values (?1, ?2, ?3)
		on conflict (story_id, uuid) do
		update set name = excluded.name
		`, name, uid, storyId)
	h.db.MustExec(ctx, "update story set updated_at = current_timestamp where story_id = ?1", storyId)
	h.success(w, fmt.Sprintf("%d\t%s", storyId, title))
}

func (h *storyHandler) handleCreate(ctx context.Context, w http.ResponseWriter, uid string, params []string) {
	if (len(params)) != 1 {
		h.error(w, "Only storyteller’s name needed")
		return
	}

	name := params[0]
	if len(name) == 0 {
		h.error(w, "name can not be empty")
		return
	}

	var storyId int
	title := generateTitle()
	for {
		row := h.db.QueryRowContext(ctx, "insert into story (title, created_by) values (?1, ?2) returning story_id", title, uid)
		if err := row.Scan(&storyId); err == nil {
			break
		} else {
			var sqlError sqlite3.Error
			if errors.As(err, &sqlError) && sqlError.ExtendedCode == sqlite3.ErrConstraintUnique {
				title = generateTitle()
			} else {
				panic(err)
			}
		}
	}
	h.db.MustExec(ctx, "insert into storyteller(name, uuid, story_id) values (?1, ?2, ?3)", name, uid, storyId)
	h.success(w, fmt.Sprintf("%d\t%s", storyId, title))
}

func (h *storyHandler) handleWrite(ctx context.Context, w http.ResponseWriter, uid string, params []string) {
	if (len(params)) != 2 {
		h.error(w, "Need story id and content")
		return
	}

	storyId, err := strconv.Atoi(params[0])
	if err != nil {
		h.error(w, "Story id is not an integer")
		return
	}

	content := strings.TrimSpace(params[1])
	if len(content) == 0 {
		h.error(w, "Content can not be empty")
		return
	}
	if len(content) > 160 {
		h.error(w, "Content must be 160 characters or less")
		return
	}

	row := h.db.QueryRowContext(ctx, "select status, created_by = ?2 from story where story_id = ?1", storyId, uid)
	var status string
	var mine bool
	if err = row.Scan(&status, &mine); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			h.error(w, "Story does not exist")
		} else {
			panic(err)
		}
		return
	}
	switch status {
	case "P":
		if !mine {
			h.error(w, "Not your turn yet")
			return
		}
		h.db.MustExec(ctx, `
			insert into part (seq, storyteller_id, content, written_at) 
			select row_number() over (order by round, sort) as seq, storyteller_id, content, written_at
			from (
					select storyteller_id, ?3 as content, current_timestamp as written_at, 0 as round, 0 as sort
					from storyteller
					where story_id = ?1 and uuid = ?2
				union all
					select storyteller_id, null, null, 1, random()
					from storyteller
					where story_id = ?1 and uuid <> ?2
				union all
					select storyteller_id, null, null, case when uuid = ?2 then 2 else 3 end, random()
					from storyteller where story_id = ?1 
				union all
					select storyteller_id, null, null, case when uuid = ?2 then 4 else 5 end, random()
					from storyteller where story_id = ?1
			) as a;
		`, storyId, uid, content)
		h.db.MustExec(ctx, "update story set status = 'S', updated_at = current_timestamp where story_id = ?1", storyId)
	case "S":
		var partId int
		var myTurn bool
		part := h.db.QueryRowContext(ctx, `
			select part_id
				 , uuid = ?2
			from part
			    join storyteller using (storyteller_id)
			where story_id = ?1
			  and content is null
			order by seq
			limit 1
		`, storyId, uid)
		err = part.Scan(&partId, &myTurn)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				h.error(w, "Not your turn yet")
				return
			} else {
				panic(err)
			}
		}
		if !myTurn {
			h.error(w, "Not your turn yet")
			return
		}
		h.db.MustExec(ctx, "update part set content = ?2, written_at = current_timestamp where part_id = ?1", partId, content)
		h.db.MustExec(ctx, `
			update story
			set status = case when exists(select 1 from part join storyteller using (storyteller_id) where story_id = ?1 and content is null) then 'S' else 'D' end
			  , updated_at = current_timestamp
			where story_id = ?1
		`, storyId)
	case "D":
		h.error(w, "This story is already finished")
		return
	}
	h.success(w, "done")
}

func (h *storyHandler) handleCheck(ctx context.Context, w http.ResponseWriter, uid string, params []string) {
	if (len(params)) != 1 {
		h.error(w, "Only last seen time needed")
		return
	}

	lastSeen, err := strconv.Atoi(params[0])
	if err != nil {
		h.error(w, "Last seen time is not an integer")
		return
	}

	timeout := 7 * time.Minute
	wait := time.Second
	conn, err := h.db.Conn(ctx)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	dataVersion := h.db.MustGetDataVersion(ctx, conn)
	for timeout > 0 {
		var written bool
		written, lastSeen = h.writeUpdated(w, ctx, uid, lastSeen)
		if written {
			break
		}
		time.Sleep(wait)
		timeout -= wait
		for timeout > 0 {
			newDataVersion := h.db.MustGetDataVersion(ctx, conn)
			if newDataVersion != dataVersion {
				dataVersion = newDataVersion
				break
			}
			time.Sleep(wait)
			timeout -= wait
		}
	}
	h.success(w, fmt.Sprintf("%d", lastSeen))
}

func (h *storyHandler) writeUpdated(w io.Writer, ctx context.Context, uid string, lastSeen int) (bool, int) {
	count := 0
	rows, err := h.db.QueryContext(ctx, `
		select story_id
		     , title
		     , status
		     , created_by = ?2
		     , unixepoch(updated_at)
		from story
		join storyteller using(story_id)
		where updated_at > datetime(?1, 'unixepoch')
		  and storyteller.uuid = ?2
		order by updated_at
	`, lastSeen, uid)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		count++
		var storyId int
		var title string
		var status string
		var mine bool
		if err = rows.Scan(&storyId, &title, &status, &mine, &lastSeen); err != nil {
			panic(err)
		}
		var owner = "O"
		if mine {
			owner = "M"
		}
		switch status {
		case "P":
			fmt.Fprintf(w, "%d\t%s\t%s\t%s\t%s\n", storyId, owner, title, status, h.buildStorytellerList(ctx, uid, storyId))
		case "S":
			fmt.Fprintf(w, "%d\t%s\t%s\t%s\t%s\n", storyId, owner, title, status, h.getLastPart(ctx, uid, storyId))
		case "D":
			fmt.Fprintf(w, "%d\t%s\t%s\t%s\t%s\n", storyId, owner, title, status, h.buildWholeStory(ctx, uid, storyId))
		}
	}
	if err = rows.Err(); err != nil {
		panic(err)
	}
	return count > 0, lastSeen
}

func (h *storyHandler) buildWholeStory(ctx context.Context, uid string, storyId int) string {
	rows, err := h.db.QueryContext(ctx, `
		select name
		     , uuid = ?2
		     , content
		from part
		join storyteller using(storyteller_id)
		where story_id = ?1
		order by seq
	`, storyId, uid)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var sb strings.Builder
	first := true
	for rows.Next() {
		var name string
		var mine bool
		var content string
		if err = rows.Scan(&name, &mine, &content); err != nil {
			panic(err)
		}
		if !first {
			sb.WriteRune('\x1e')
		}
		sb.WriteString(name)
		sb.WriteRune('\x1f')
		if mine {
			sb.WriteRune('M')
		} else {
			sb.WriteRune('O')
		}
		sb.WriteRune('\x1f')
		sb.WriteString(content)
		first = false
	}
	if err = rows.Err(); err != nil {
		panic(err)
	}
	return sb.String()
}

func (h *storyHandler) buildStorytellerList(ctx context.Context, uid string, storyId int) string {
	rows, err := h.db.QueryContext(ctx, `
		select name
		from storyteller
		where story_id = ?1
		  and uuid <> ?2
		order by name
	`, storyId, uid)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var sb strings.Builder
	first := true
	for rows.Next() {
		var name string
		if err = rows.Scan(&name); err != nil {
			panic(err)
		}
		if !first {
			sb.WriteRune('\x1e')
		}
		sb.WriteString(name)
		first = false
	}
	if err = rows.Err(); err != nil {
		panic(err)
	}
	return sb.String()
}

func (h *storyHandler) getLastPart(ctx context.Context, uid string, storyId int) string {
	part := h.db.QueryRowContext(ctx, `
			select uuid = ?2
			from part
			    join storyteller using (storyteller_id)
			where story_id = ?1
			  and content is null
			order by seq
			limit 1
		`, storyId, uid)
	var myTurn bool
	if err := part.Scan(&myTurn); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return ""
		} else {
			panic(err)
		}
	}
	if !myTurn {
		return ""
	}
	return h.db.MustGetString(ctx, `
		select content
		from part
		    join storyteller using(storyteller_id)
		where story_id = ?1
		  and content is not null
		order by seq desc
		limit 1
	`, storyId, uid)
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	if i := strings.IndexByte(p[1:], '/') + 1; i <= 0 {
		return p[1:], "/"
	} else {
		return p[1:i], p[i:]
	}
}

func setSessionCookie(w http.ResponseWriter, cookie string) {
	http.SetCookie(w, createSessionCookie(cookie, 8766*24*time.Hour))
}

func createSessionCookie(value string, duration time.Duration) *http.Cookie {
	return &http.Cookie{
		Name:     sessionCookie,
		Value:    value,
		Path:     "/",
		Expires:  time.Now().Add(duration),
		HttpOnly: true,
		SameSite: http.SameSiteNoneMode,
		Secure:   true,
	}
}

func methodNotAllowed(w http.ResponseWriter, _ *http.Request, allowed ...string) {
	w.Header().Set("Allow", strings.Join(allowed, ", "))
	http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
}

func recoverPanic(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				if r == http.ErrAbortHandler {
					panic(r)
				}
				//goland:noinspection GoTypeAssertionOnErrors
				err, ok := r.(error)
				if !ok {
					err = fmt.Errorf("%v", r)
				}
				stack := make([]byte, 4<<10)
				length := runtime.Stack(stack, true)
				log.Printf("PANIC - %v %s", err, stack[:length])
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func fatalf(format string, a ...any) {
	_, _ = fmt.Fprintln(os.Stderr, fmt.Errorf(format, a...))
	os.Exit(1)
}
